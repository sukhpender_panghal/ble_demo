package com.example.ble_demo

import android.bluetooth.*
import android.bluetooth.le.*
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import java.io.UnsupportedEncodingException
import java.net.URLEncoder


class MainActivity : AppCompatActivity() {

    val SCAN_PERIOD: Long = 200
    var mLEScanner: BluetoothLeScanner? = null
    var settings: ScanSettings? = null
    var filters: List<ScanFilter>? = null
    var mBluetoothAdapter: BluetoothAdapter? = null
    val lis = ArrayList<BluetoothDevice?>()
    val handler = Handler()
    private var mGatt: BluetoothGatt? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        locationPermission()
        setupConnection()
        val bluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        mBluetoothAdapter = bluetoothManager.adapter
        onClick()
    }

    private fun onClick() {
        btn_BT_scan.setOnClickListener {
            lis.clear()
            scanLeDevice(true)
        }
    }

    fun locationPermission() {
        if (ContextCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION
                )
            ) else {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), 1
                )
            }
        }
    }

    fun setupConnection() {
        val bluetoothAdapter: BluetoothAdapter? = BluetoothAdapter.getDefaultAdapter()
        if (bluetoothAdapter == null) {
            Toast.makeText(getApplicationContext(), "Bluetooth Not Supported", Toast.LENGTH_SHORT)
                .show()
            // Device doesn't support Bluetooth
        } else {
            if (bluetoothAdapter.isEnabled == false) {
                val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                startActivityForResult(enableBtIntent, 1)
            }
        }
    }

    private fun scanLeDevice(enable: Boolean) {
        mLEScanner = mBluetoothAdapter?.bluetoothLeScanner
        settings = ScanSettings.Builder()
            .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
            .build()
        filters = ArrayList()
        if (enable) {
            handler?.postDelayed({
                mLEScanner?.stopScan(mScanCallback)
            }, SCAN_PERIOD)
            mLEScanner?.startScan(filters, settings, mScanCallback)
            Log.e("scan", "" + filters.toString())
        } else {
            mLEScanner?.stopScan(mScanCallback)
        }
    }

    private val mScanCallback: ScanCallback = object : ScanCallback() {
        override fun onScanResult(callbackType: Int, result: ScanResult) {
            val btDevice: BluetoothDevice? = result.device

            lis.add(btDevice)
            Log.e("data1", "" + lis)
            recycler_view.layoutManager = LinearLayoutManager(this@MainActivity)
            val adapt = CustonDeviceScanAdapter(this@MainActivity, lis) {
                connectToDevice(btDevice)
            }
            adapt.notifyDataSetChanged()
            recycler_view.adapter = adapt
        }
    }

    fun connectToDevice(device: BluetoothDevice?) {
        if (mGatt == null) {
            mGatt = device?.connectGatt(this, false, gattCallback)
            scanLeDevice(false) // will stop after first device detection
        }
    }

    private val gattCallback: BluetoothGattCallback = object : BluetoothGattCallback() {
        override fun onConnectionStateChange(gatt: BluetoothGatt, status: Int, newState: Int) {
            Log.e("onConnectionStateChange", "Status: $status")
            when (newState) {
                BluetoothProfile.STATE_CONNECTED -> {
                    Log.i("gattCallback", "STATE_CONNECTED")
                    gatt.discoverServices()
                }
                BluetoothProfile.STATE_DISCONNECTED -> Log.e("gattCallback", "STATE_DISCONNECTED")
                else -> Log.e("gattCallback", "STATE_OTHER")
            }
        }

        override fun onServicesDiscovered(gatt: BluetoothGatt, status: Int) {
            val services = gatt.services

            Log.i("onServicesDiscovered", services.toString())
            gatt.readCharacteristic(services[1].characteristics[0])
        }

        override fun onCharacteristicRead(
            gatt: BluetoothGatt,
            characteristic: BluetoothGattCharacteristic, status: Int
        ) {
            Log.i("onCharacteristicRead", characteristic.toString())
            val a = characteristic.writeType
            val b = characteristic.descriptors
            Log.e("asd",""+b)
            gatt.disconnect()
        }
    }

    fun writeCharacteristic(characteristic: BluetoothGattCharacteristic, data: String?): Boolean {

        if (mGatt == null) {
            Log.e("asd", "lost connection")
        }
        Log.e("asd", "characteristic $characteristic")
        try {
            Log.e("asd", "data " + URLEncoder.encode(data, "UTF-8"))
            characteristic.setValue(URLEncoder.encode(data, "UTF-8"))

            mGatt!!.writeCharacteristic(characteristic)
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        }
        val value = ByteArray(1072)
        value[0] = (21 and 0xFF).toByte()
        characteristic.value = value
        return mGatt!!.writeCharacteristic(characteristic)
    }
}