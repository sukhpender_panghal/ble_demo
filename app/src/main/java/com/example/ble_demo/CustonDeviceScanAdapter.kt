package com.example.ble_demo

import android.bluetooth.BluetoothDevice
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.custom_list_design.view.*

class CustonDeviceScanAdapter(
    val context: Context,
    lis: ArrayList<BluetoothDevice?>,
    val onItemClick: (BluetoothDevice) -> Unit)
    : RecyclerView.Adapter<CustonDeviceScanAdapter.ViewHolder>() {

    val list = lis

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.custom_list_design,
            parent,
            false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        if (list[position]!!.name == null || list[position]!!.address == null) {
            holder.itemView.txt_device_name.text = ""
            holder.itemView.txt_device_address.text = ""
        } else {
            holder.itemView.txt_device_name.text = list[position]?.name
            holder.itemView.txt_device_address.text = list[position]?.address
            holder.itemView.setOnClickListener {
                onItemClick(list[position]!!)
            }
        }
    }


    class ViewHolder(view : View) : RecyclerView.ViewHolder(view)
}